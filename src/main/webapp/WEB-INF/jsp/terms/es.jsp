 <div id="policy_screen">
        <h3>WELIVE - T�RMINOS Y CONDICIONES DE USO</h3>

        <div class="policy_box">

            <div class="policy_text">

                <p class="tool_tit">1. INTRODUCCI�N: T�RMINOS Y CONDICIONES - aplicables en todo el mundo salvo en aquellos sitios que se hayan
                    publicado t�rminos y condiciones espec�ficas para dicho territorio </p>
                <p class="tool_desc">Estos t�rminos y condiciones ("T�rminos") contienen informaci�n importante acerca de los derechos, obligaciones
                    y restricci�n que se le pueden aplicar a usted como usuario ("Usuario") cuando accede a cualquiera de
                    los servicios web ("Servicios") proporcionados bajo el dominio de internet http://dev.welive.eu ("Sitio
                    Web/Plataforma/Sistema") o cuando accede, descarga y usa la aplicaci�n WeLive Player y los diferentes
                    servicios p�blicos ("Aplicaci�n") y cualquiera de los servicios que se proporcionan a trav�s de ella.
                    Todos los artefactos que forman parte del sistema, es decir: datasets, building blocks y aplicaciones,
                    gestionadas dentro de la soluci�n WeLive son gestionados por la plataforma ("Plataforma"). El WeLive
                    Player act�a como una "tienda" para aquellas aplicaciones desarrolladas dentro de la plataforma WeLive,
                    permitiendo a los Usuarios encontrar qu� aplicaciones est�n disponibles en su entorno haciendo uso de
                    informaci�n p�blica. Adem�s, es posible que desde la aplicaci�n o desde el Sitio Web pueda acceder a
                    otros artefactos ("Artefacto") como: aplicationes, building blocks y datasets. La plataforma, Sitio Web
                    y la aplicaci�n, salvo que se indique lo contrario, son proporcionados por todos los socios que conforman
                    el consorcio WeLive ("Consorcio"). El proyecto WeLive (http://www.welive.eu/) ha sido financiado por
                    el programa marco de la Uni�n Europea (H2020) dentro del acuerdo n� 645845.</p>
                <p class="tool_desc">Estos T�rminos pueden ser accedidos a trav�s de la Aplicaci�n instalada en su dispositivo o en el Sitio Web.
                </p>
                <p class="tool_desc">El uso de la Aplicaci�n, del Sitio Web y de la plataforma WeLive (Artefactos, Servicios y Plataforma) requiere
                    la aceptaci�n de los t�rminos expuestos a continuaci�n, as� como el de cualquier ley que pueda estar
                    relacionada con el uso de WeLive. El Usuario de la Plataforma puede desempe�ar el rol de consumidor de
                    los servicios de WeLive pero tambien el de producto y proveedor de nuevos servicios (Prosumer). Los nuevos
                    servicios creados deben cumplir con la pol�tical de protecci�n y acceso a los datos del pa�s donde se
                    encuentra la plataforma. Los servicios gratuitos y los datos no-privados proporcionados por el Usuario
                    y alojados en la plataforma son propiedad del Consorcio. Sin embargo, un proveedor de artefactos tiene
                    el derecho a eliminar dichos artefactos publicados previamente, independientemente si �stos est�n siendo
                    usados por otros usuarios o no. Cuando un Usuario decide eliminar el Usuario de la plataforma, sus artefactos
                    son anonimizados y permanecer�n en la plataforma a menos que el Usuario explicitamente indique que desea
                    su eliminaci�n. </p>
                <p class="tool_desc">Por favor si no est� de acuerdo con estos T�rminos o considera que futuros cambios en los mismos pueden hacer
                    que no sean aceptables para usted, no use WeLive. El uso continuado de WeLive incluyendo, pero no limit�ndose,
                    el Sitio Web, las aplicaciones, la Plataforma, los artefactos y la informaci�n actualizaciones y servicios
                    se considerar� una aceptaci�n de los estos t�rminos y cualquier cambio que se realice en los mismos.
                    Adem�s usted acepta usar WeLive con prop�sitos �nicamente legales, apropiados, acordes a estos T�rminos
                    y a las pol�ticas locales. </p>
                <p class="tool_desc">Se considerar� aceptaci�n de los T�rminos, la aceptaci�n expresa de los mismos durante el proceso de registro
                    y/o a la hora de instalar la Aplicaci�n, cuando se descarga �sta o una actualizaci�n y cuando se usa
                    la misma en un dispositivo. </p>

                <p class="tool_tit">2. POL�TICA DE USO</p>
                <p class="tool_desc">WeLive es una plataforma gratuita que permite su uso de forma no limitada. Usted se compromete a hacer un
                    uso razonable de la misma: Plataforma, Sitio Web, Servicios, Aplicaci�n y cualquier clase de Artefacto.
                    Se considera una violaci�n de los T�rminos la consulta o escritura de datos excesiva de tal forma que
                    el sistema se quede saturado o afectado no pudiendo dar servicio a otros usuarios. </p>
                <p class="tool_desc">El Sitio Web, Aplicaci�n y Plataforma permite la publicaci�n de informaci�n. El Usuario se hace responsable
                    de toda la informaci�n, opini�n, comentario, etc. que publique en WeLive directamente o a trav�s del
                    Sitio Web, Aplicaci�n y Plataforma.</p>
                <p class="tool_desc">El Usuario se compromete a no publicar, ni fomentar la publicaci�n de: </p>
                <ul>
                    <li> informaci�n injuriosa, discriminatoria, vejatoria, obscena, etc. o cualquier otra que pueda considerarse
                        ofensiva en t�rminos relativos a raza, religi�n, etnia, sexo; </li>
                    <li> Material que incite a la comisi�n de delitos; </li>
                    <li> material del que no se sea titular y no se disponga de autorizaci�n para publicarlo o que infrinja el
                        copyright, patentes, marcas registradas; </li>
                    <li> publicidad, spam, esquemas piramidales; </li>
                    <li> informaci�n que tenga como objetivo enga�ar a otros; </li>
                    <li> material que explote o permita explotar brechas de seguridad; </li>
                    <li> material que contenga virus, troyanos u otro tipo de c�digo malicioso que afecte al funcionamiento del
                        servicio o a los usuarios; </li>
                    <li> material que acceda o intente acceder a las cuentas de otros usuarios; </li>
                    <li> material que recopile informaci�n de otros usuarios; </li>
                    <li> todo aquello que incumpla la ley local vigente. </li>
                </ul>
                <p class="tool_desc">En algunos casos, puede ser necesario el registro en el Sitio Web y/o en alguna de las aplicaciones desarrolladas
                    y alojadas en la plataforma WeLive para poder usar la aplicaci�n. El Usuario se compromete a proporcionar
                    informaci�n actual y precisa para formalizar el registro y siempre que se le solicite. Esta informaci�n
                    adem�s se deber� mantener actualizada. En caso de existir la sospecha de usar informaci�n falsa o inexacta
                    se podr� suspender la cuenta. En el caso que el Usuario decida eliminar el registro previo, todos sus
                    artefactos permanecer�n an�nimos; es decir, sin ning�n tipo de relaci�n con el Usuario cuyo registro
                    ha sido borrado en la Plataforma. </p>
                <p class="tool_desc">La plataforma WeLive proporciona una API p�blica con un conjunto de m�todos a trav�s de los cuales se puede
                    modificar el estado de los artefactos dados de alta en el sistema. �nicamente a los usuarios que se hayan
                    registrado previamente en la plataforma y cuyo perfil exista en el Citizen Data Vault ("CDV") se les
                    permite ejecutar dichas operaciones.</p>
                <p class="tool_desc">La violaci�n de la pol�tica de uso supondr� la cancelaci�n inmediata de la cuenta de usuario y el rechazo
                    de la creaci�n futuras cuentas y la de aplicaci�n de los medio para impedir el uso de la Aplicaci�n,
                    Plataforma y/o Sitio Web. </p>
                <p class="tool_desc">En cualquier momento se pueden modificar los servicios, o suprimir parte de ellos. Los cambios ser�n notificados
                    de forma razonable. Seguir usando los servicios ser� entendido como una aceptaci�n de los cambios y de
                    los t�rminos y condiciones. </p>

                <p class="tool_tit">3. PAGO POR EL SERVICIO</p>
                <p class="tool_desc">El Sitio Web, Aplicaci�n, Plataforma, API p�blica, Servicios y Artefactos proporcionados por WeLive se ofrecen
                    y proporcionan sin ning�n coste durante las dos fases de ejecuci�n del Piloto del proyecto. En otro �mbito,
                    la utilizaci�n de Aplicaciones, Servicios y Artefactos proporcionados por terceras partes puede implicar
                    alg�n tipo de coste. Los pagos requeridos para la utilizaci�n de los servicios proporcionados por terceras
                    partes son cargados de forma directa por el proveedor del servicio y fuera de la Plataforma WeLive y
                    WeLive, en este caso, no es responsable de nuinguna manera de estas transacciones. </p>
                <p class="tool_desc">En caso de que en un futuro se decidiese cobrar por el uso del servicio o ciertas partes de �l, se comunicar�a
                    de forma razonable, as� como las tarifas, permitiendo a los usuarios que no quieran usar esos servicios
                    de pago mantener los servicios gratuitos o dejar de usar el servicio proporcionado antes de empezar a
                    facturar por el uso. </p>
                <p class="tool_desc">El uso de la Plataforma, Servicios y Aplicaci�n implica un acceso a red lo que puede suponer costes adicionales
                    para el usuario cuando lo hace tanto desde dispositivos m�viles como desde dispositivos fijos por el
                    uso de tarifas de red o de datos. Estos costes no son en ning�n caso atribuibles a WeLive y ser�n asumidos
                    por el Usuario. </p>

                <p class="tool_tit">4. PRIVACIDAD DE LOS DATOS</p>
                <p class="tool_desc">Antes de comenzar a usar el Servicio, los Usuarios deben aprobar el documento de T�rminbos y Condiciones
                    de Uso, que ser� mostrado a los Usuarios la primera vez que lanzan las Aplicaciones y/o se registran
                    en el Sitio Web y Plataforma. Cuando usted se registra en el Sitio Web y/o en alguna de las aplicaciones
                    para su uso, proporciona una serie de datos personales (no sensibles) que espera se gestionen de una
                    forma correcta. El Sitio Web en ning�n caso le solicitar� datos sensibles (para m�s informaci�n acerca
                    de qu� se considera datos sensibles, por favor, revisen el art�culo 8 de la Directiva Europea 95/46/EC
                    - https://secure.edps.europa.eu/EDPSWEB/webdav/site/mySite/shared/Documents/EDPS/DataProt/Legislation/Dir_1995_46_EN.pdf).
                    TECNALIA y el resto del consorcio de WeLive tratar� esos datos adecuadamente y siempre de acuerdo a las
                    exigencias de la legislaci�n local vigente o, en su ausencia, la legislaci�n europea para la protecci�n
                    de los datos que tendr� efecto en el a�o 2018. As� mismo el Usuario deber� proteger su cuenta usando
                    los medios que se le proporcionan para ello como son, sin limitarse a ellos, el uso de una contrase�a
                    adecuada, el cambio peri�dico de la misma, o no compartir los datos de la cuenta. TECNALIA y el resto
                    del consorcio de WeLive usar� los datos proporcionados por los Usuarios para gestionar y monitorizar
                    el uso del Sitio Web, Plataforma, Aplicaciones y los artefactos de WeLive. En ning�n momento negociar�
                    con los datos y los mismos ser�n utilizados �nicamente para la gesti�n del uso del Sitio Web. </p>
                <p class="tool_desc">TECNALIA y el resto del consorcio de WeLive recopilar�n informaci�n personal de los Usuarios, siempre sujeta
                    a autorizaci�n previa por parte del Usuario, durante su uso del Servicio. Estos datos se almacenar�n
                    en el Citizen Data Vaulto ("CDV"), un componente encargado de asegurar y controllar el acceso a los datos
                    previa autorizaci�n. S� se podr� recopilar informaci�n estad�stica an�nima sobre el uso del servicio
                    para tratar de mejorar el mismo y hacer una evaluaci�n del uso de cada uno de los servicios. De hecho,
                    dentro de esta informaci�n anonimizada, creada internamente por los componentes de WeLive, los usuarios
                    son identificados mediante un identificador �nico solamente conocido por el sistema WeLive. Esto permite
                    recuperar informaci�n sobre la ultilizacion de las distintas herramientas y artefactos que forman parte
                    de WeLive y ser� usada para generar m�tricas y visualizaciones de �stas que permitan al proyecto evaluar
                    en tiempo real la acepetacion de la plataforma y de los servicios. En ning�n caso estas m�tricas y visualizaciones
                    revelar�n los Identificadores de los usuarios. Esta informaci�n puede, tambi�n, ser utilizada para el
                    env�o de recomendaciones personalizadas de acuerdo al perfil del usuario. Adem�s, los datos personales
                    (correo electr�nico) ser� unicamente usado para establecer comunicaci�n directa con los Usuarios durante
                    el Piloto. Los datos personales nunca ser�n revelados a terceras partes a menos que el Usuario explicitamente
                    lo consienta. El Sitio Web expl�citamente no recopila datos sensibles sobre los usuarios. Cualquier Aplicaci�n
                    que recopile y/o use datos sensibles no est� autorizada a usar ninguno de los componentes de WeLive para
                    el almacenamiento de dichos datos sensibles. La plataforma WeLive no permite que datos personales sensibles
                    de los usuarios sean almacenados en la misma. Como datos sensibles consideramos aquellos incluidos en
                    el siguiente documento oficial de la UE1. Los usuarios de cualquier aplicaci�n compatible con el sistema
                    WeLive deben cumplir con esta restricci�n relativa al almacenamiento de datos personales sensibles dentro
                    de WeLive. </p>
                <p class="tool_desc">Los Usuarios pueden personalizar la Aplicaci�n y las b�squedas mediante el uso de filtros. Estos filtros
                    solo se guardar�n en el dispositivo del usuario y en ning�n momento viajar�n por la red o ser�n recopilados
                    por ning�n motivo. </p>

                <p class="tool_tit">5. PROPIEDAD DE LOS DATOS</p>
                <p class="tool_desc">Los datos a los que el usuario accede a trav�s del Sitio Web, Plataforma, Aplicaci�n y de cualquier Artefacto
                    provienen de fuentes p�blicas o sus propietarios los han puesto a disposici�n del p�blico para su uso
                    y por tanto mantienen su propiedad y el control de los mismos conjuntamente con el gestor de la Plataforma.
                    En WeLive, es posible el almacenamiento de datos privados donde �nicamente Usuarios autorizados tendr�n
                    acceso a los mismos. </p>
                <p class="tool_desc">Los datos se ponen a la disposici�n del Usuario para su consulta. No se permite la recopilaci�n o la creaci�n
                    de bases de datos a partir de estos. </p>
                <p class="tool_desc">El acceso de los datos se ofrece sin ning�n coste. El desarrollo de trabajos derivados que accedan a estos
                    datos deber� mantener siempre el acceso a ellos sin coste. </p>
                <p class="tool_desc">Se permite a los Usuarios la creaci�n, actualizaci�n, modificaci�n e inclusi�n de nuevos datos en la Plataforma
                    y/o Aplicaci�n and se asegura el acceso completo a los mismos. Durante la creaci�n de los datos, los
                    Usuarios deben especificar si �stos datos ser�n de dominio p�blico (por defecto) o privado. Los datos
                    generados por el usuario, en el caso de que no sean de naturaleza personal y/o privados, pertenecer�n
                    tanto al Usuario como a la organizaci�n encargada del alojamiento de la Plataforma (Consorcio). Los Usuarios,
                    en todo momento, mantendr�n el control sobre los datos que hayan creados y ser�n capaces de eliminarlos.
                </p>
                <p class="tool_desc">Los datos generados por los usuarios sobre datasets de car�cter p�blico son compartidos en la plataforma
                    y el usuario no tendr� la posibilidad de borrarlos a menos que incluyan datos de car�cter personal o
                    privado. Cualquier rastro, como por ejemplo los Identificadores de Usuario, que pueda ser usado para
                    vincular a usuarios que quieren desregistrarse de la plataforma a trav�s de los datos que crearon previamente
                    ser�n anonimizados vincul�ndolos a un ID gen�rico. En cualquier momento, el Usuario dispondr� de acceso
                    a los datos a traves del "CDV" desde donde podr� extraer todos sus datos personales. Del mismo modo,
                    a trav�s del Open Data Stack ("ODS"), se permitir� al usuario a extraer los contenidos de los datos que
                    le pertenecen. Los nuevos datos creados ser�n de dominio p�blico al igual que son los datos con los que
                    est�n trabajando y usando. </p>
                <p class="tool_desc">En el caso de que tenga la intenci�n de publicar datos en WeLive que ya se encuentren disponibles en otros
                    sitios web o redes sociales, por favor refi�rase a la pol�tica de privacidad de dichos sitios antes de
                    publicar con el fin de asegurarse el derecho a poder hacerlo. Asimismo, en el caso de tener la intenci�n
                    de publicar datos de WeLive en otros sitios web, por favor, comprueben primero la pol�tica de privacidad
                    para asegurarse de que los presentes T�rminos y Condiciones de Uso no son incumplidos. En el cao de republicar
                    datos, es necesarioa�adir una referencia al origen de los datos. </p>
                <p class="tool_desc">El consorcio WeLive no pagar� ninguna contraprestaci�n econ�mica por los datos proporcionados. TECNALIA y
                    el resto del consorcio de WeLive se reserva el derecho de revisar los datos aportados y eliminarlos en
                    caso de considerar que no son correctos o que violan el prop�sito del sitio. El componente del sistema
                    denominado "Open Data Stack" proporciona los mecanismos necesarios para identificar aquellos datos que
                    podr�an ser usados de forma incorrecta. </p>
                <p class="tool_desc">Una vez que el proyecto concluya, los datos publicados podr�an tener un coste asociado a su consumo siempre
                    y cuando el creador de estos datos as� determine. Por favor, si desea retener el copyright de sus datos
                    u obtener alguna contraprestaci�n por ellos es preferible que no los publique.</p>


                <p class="tool_tit">6. EXACTITUD DE LOS DATOS</p>
                <p class="tool_desc">La informaci�n se proporciona tal cual la proporcionan las fuentes de datos; es decir, los valores/contenidos
                    no son modificados, aunque el formato en el que son exportados podr�a ser modificado para facilitar su
                    explotaci�n posterior. TECNALIA y el resto del consorcio de WeLive no se hace responsable de la exactitud
                    de la misma ni de los da�os y costes directos o indirectos, causados por el uso de esta informaci�n.
                    La Plataforma proporciona herramientas para autom�ticamente evaluar la calidad de los datos y verificar
                    su exactitud. </p>

                <p class="tool_tit">7. TERCERAS PARTES</p>
                <p class="tool_desc">Es posible que desde el Sitio Web, Plataforma o desde cada una de las Aplicaciones se pueda acceder a otros
                    sitios y redes sociales, ajenas a TECNALIA y el resto del consorcio de WeLive y gestionados por terceras
                    partes. TECNALIA y el resto del consorcio de WeLive no se hace responsable de las pol�ticas de privacidad
                    de estos sitios y de los posibles costes asociados a su utilizaci�n. </p>
                <p class="tool_desc">En caso de usar la cuenta de redes sociales para registrarse en el Sitio Web, Plataforma y/o para el uso
                    de la aplicaci�n, TECNALIA y el resto del consorcio de WeLive no recolectar� m�s informaci�n que la estrictamente
                    necesaria para gestionar la cuenta y el acceso a la misma. </p>
                <p class="tool_desc">La publicaci�n de informaci�n en redes sociales, a trav�s del Sitio Web, Plataforma y/o Aplicaci�n, estar�
                    regulada por la pol�tica de privacidad y los t�rminos y condiciones de uso de dicha red social. </p>

                <p class="tool_tit">8. PROPIEDAD</p>
                <p class="tool_desc">El Sitio Web, Plataforma, Aplicaci�n y Artefactos de WeLive es propiedad del Consorcio. Cualquier otro Artefacto
                    es propiedad de su autor y del gestor de la plataforma; es decir, la organizaci�n encargada del alojamiento
                    de la misma. La explotaci�n de Artefactos proporcionados por terceras partes, en el caso de que se haya
                    configurado un precio por uso del mismo, ser� compratido por los autores y la organizaci�n encargada
                    del alojamiento de la Plataforma. </p>
                <p class="tool_desc">Se concede al usuario una licencia no exclusiva, revocable para copiar, instalar y usar la aplicaci�n en
                    todos sus dispositivos y para usar el Servicio sin ning�n coste. Esto no otorga al usuario ning�n derecho
                    sobre la propiedad de la Aplicaci�n y Artefactos que seguir�n siendo de los autores. </p>

                <p class="tool_tit">9. DISPONIBILIDAD</p>
                <p class="tool_desc">TECNALIA y el resto del consorcio de WeLive no adquieren ning�n compromiso de disponibilidad del Sitio Web,
                    Aplicaci�n, Plataforma y Artefactos y/o de la informaci�n asociada a los mismo. En cualquier caso pondr�
                    todo su esfuerzo para evitar o minimizar los tiempos de interrupci�n y que el tiempo de servicio sea
                    el m�ximo posible. Siempre que sea posible, se notificar�n las paradas programadas del sitio que vayan
                    a afectar de forma rese�able a la disponibilidad del servicio. </p>
                <p class="tool_desc">TECNALIA y el resto del consorcio de WeLive no se hace responsable de los da�os y los costes directos o indirectos
                    que se puedan causar por la no disponibilidad del sitio. </p>

                <p class="tool_tit">10. PROTECCI�N LEGAL Y LIMITACIONES</p>
                <p class="tool_desc">Se concede al usuario una licencia no exclusiva, revocable para copiar, instalar y usar la aplicaci�n en
                    todos sus dispositivos y para usar el Servicio sin ning�n coste. </p>
                <p class="tool_desc">Usted acepta no usar la aplicaci�n para: </p>
                <ul>
                    <li> interferir o manipular el Servicio; </li>
                    <li> alterar los datos de forma maliciosa; </li>
                    <li> recopilar datos de los usuarios de la Aplicaci�n; </li>
                    <li> amenazar, molestar, chantajear a otros usuarios; </li>
                    <li> interferir en los servicios de terceras partes; </li>
                    <li> afectar a los copyright de terceras partes.</li>
                </ul>
                <p class="tool_desc">La Aplicaci�n se proporciona "tal cual" y usted la usa bajo su propia responsabilidad. TECNALIA y el resto
                    del consorcio de WeLive se exime de la responsabilidad por errores, mal funcionamiento en su terminal,
                    si afecta a otras aplicaciones, p�rdida de datos, p�rdida de disponibilidad por el uso de la misma. </p>

                <p class="tool_tit">11. INTERPRETACI�N</p>
                <p class="tool_desc">El presente documento "T�rminos y Condiciones de Uso" del ecosistema WeLive ha sido escrito en Ingl�s y traducido
                    a los diferentes idiomas oficiales del proyecto pensando en el beneficio del usuario final. El usuario
                    puede acceder a las diferentes versiones seleccionando los diferentes idiomas existentes dentro de la
                    opcion "Lenguaje" del sitio web. En el caso de que exista alguna incoherencia o conflicto entre las diferentes
                    versiones de este documento, la versi�n en ingl�s es la que prevalece con respecto al resto de versiones.</p>

            </div>

        </div>

    </div>
