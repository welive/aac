
<div id="policy_screen">
	<h3>WELIVE - TERMS AND CONDITIONS OF USE</h3>

	<div class="policy_box">
		<div class="policy_text">

			<p class="tool_tit">1. JOHDANTO: EHDOT JA EDELLYTYKSET - Sovelletaan maailmanlaajuisesti paitsi kun julkaisemme erityisi� alueellisia
                    ehtoja </p>
                <p class="tool_desc">N�m� k�ytt�ehdot ("Ehdot") kertovat t�rke�� tietoa oikeuksista, velvollisuuksista ja rajoitteista, jotka
                    voivat koskea sinua k�ytt�j�n� ("K�ytt�j�") kun k�yt�t mit� tahansa https://dev.welive.eu-toimialueen
                    ("Verkkosivu") verkkopalveluista ("Palvelu") p��st�ksesi k�siksi, ladataksesi tai k�ytt��ksesi WeLive
                    Player -sovellusta ja muita julkisen palvelun sovelluksia ("Sovellus") tai mit� tahansa muita sen tarjoamia
                    palveluita. Kaikki WeLive-palvelun artefaktit eli tietoaineistot, rakennuspalikat (tai koottavat mikropalvelut)
                    ja sovellukset noudattavat sen viitekehyst� ("Viitekehys"). WeLive Player -sovellus toimii markkinapaikkana
                    WeLive-viitekehyksess� kehitetyille sovelluksille, ja sen avulla k�ytt�j�t l�yt�v�t saatavilla olevat
                    julkista dataa hy�dynt�v�t sovellukset. Lis�ksi verkkosivu mahdollistaa p��syn muihin artefakteihin ("Artefakti")
                    kuten julkisten palveluiden sovelluksiin, rakennuspalikoihin ja tietoineistoihin. Ellei toisin mainita,
                    Viitekehys, Verkkosivu ja Sovellus ovat kaikkien WeLive-konsortion ("Konsortio") muodostavien partnerien
                    tarjoamia. WeLive-hanke (http://www.welive.eu) on saanut rahoitusta Euroopan unionin H2020-ohjelmasta
                    tutkimukseen, teknologiseen kehitykseen ja demonstraatioon apurahasopimuksen Nro 645845 mukaisesti. Hanke
                    kest�� 36 kuukautta helmikuusta 2015 tammikuuhun 2018.</p>
                <p class="tool_desc">N�m� k�ytt�ehdot ovat saatavilla laitteeseesi asennetussa sovelluksessa tai verkkosivulla. </p>
                <p class="tool_desc">Sinun on hyv�ksytt�v� ehdot ja muut soveltuvat lait, jotka koskevat niiden palveluiden k�ytt��, jotka WeLive
                    tarjoaa komponenttiensa ja artefaktiensa (Artefakti, Verkkosivu ja Viitekehys) kautta. K�ytt�j� voi olla
                    WeLive-palveluiden kuluttaja, mutta my�s palveluiden tuottaja. Tuotettujen palveluiden on noudatettava
                    WeLive-infrastruktuuria yll�pit�v�n maan dataa koskevaa lains��d�nt��. K�ytt�j�n tarjoamat ja Viitekehyksess�
                    yll�pidetyt vapaat palvelut ja ei-yksityinen tieto ovat Konsortion omaisuutta. Artefaktin tarjoajalla
                    on kuitenkin oikeus poistaa omat julkaistut artefaktinsa riippumatta siit� k�ytt�v�tk� muut niit� vai
                    eiv�t. Kun k�ytt�j� lakkauttaa tilins� Viitekehyksest�, h�nen artefaktinsa anonymisoidaan ja ne s�ilyv�t
                    Viitekehyksess� ellei k�ytt�j� selv�sti osoita haluavansa ne poistettavan. </p>
                <p class="tool_desc">Jollet hyv�ksy n�it� ehtoja tai ep�ilet mahdollisten tulevien muutosten tekev�n ne sinulle mahdottomiksi,
                    eth�n k�yt� WeLive�. Jos k�yt�t WeLive�, mukaan lukien Verkkosivua, Sovellusta, Viitekehyst�, rakennuspalikoita,
                    tietoaineistoa, julkisten palveluiden sovelluksia, p�ivityksi� jne., katsomme sinun hyv�ksyv�n n�m� Ehdot
                    ja niihin tekem�mme muutokset. Sitoudut k�ytt�m��n WeLive� ainoastaan tarkoituksiin, jotka ovat laillisia,
                    asiallisia ja n�iden Ehtojen ja muiden soveltuvien s��nn�sten mukaisia. </p>
                <p class="tool_desc">Katsomme sinun hyv�ksyv�n Ehdot, kun selv�sti hyv�ksyt ne rekister�intiprosessin ja/tai Sovelluksen asennusprosessin
                    aikana, kun lataat tai p�ivit�t sen tai kun k�yt�t sit� laitteellasi. </p>

                <p class="tool_tit">2. K�YTT�S��NN�T</p>
                <p class="tool_desc">WeLive on ilmainen avoimen l�hdekoodin viitekehys, joten sen k�ytt�� ei ole rajoitettu. Sitoudut k�ytt�m��n
                    Verkkosivua, Viitekehyst� ja Sovellusta sek� muita artefakteja j�rkev�sti. J�rjestelm�n ruuhkauttaminen
                    tai sen toimintaan vaikuttaminen lukemalla tai kirjoittamalla dataa niin ett� se vaikuttaa Palvelun muihin
                    k�ytt�jiin on Ehtojen vastaista. </p>
                <p class="tool_desc">Verkkosivu ja Sovellus antavat sinun julkaista joitakin tietoja. Olet vastuussa tiedoista, mielipiteist�,
                    kommenteista jne. jotka ilmaiset WeLivess� suoraan tai k�ytt�m�ll� Verkkosivua, Sovellusta ja/tai Palvelua.
                </p>
                <p class="tool_desc">K�ytt�j� sitoutuu olemaan julkaisematta tai olemaan rohkaisematta julkaisemaan: </p>
                <ul>
                    <li> tietoja, jotka ovat loukkaavia, syrjivi�, n�yryytt�vi�, s��dytt�mi� jne. tai jotka voivat loukata muita
                        rodun, uskonnon, sukupuolen tai etnisyyden perusteella; </li>
                    <li> mit��n rikoksen tekoon yllytt�v�� materiaalia; </li>
                    <li> mit��n materiaalia, joka ei ole sinun, jonka julkaisuun sinulla ei ole lupaa tai joka loukkaa tekij�noikeuksia
                        tai patentteja; </li>
                    <li> mainoksia, roskapostia, pyramidihuijauksia; </li>
                    <li> tietoja muiden k�ytt�jien huijaamiseksi; </li>
                    <li> tietoja turvallisuusaukkojen hy�dynt�misest�; </li>
                    <li> tiedostoja, jotka sis�lt�v�t viruksia, troijalaisia tai muita haittaohjelmia, jotka voivat vaikuttaa
                        Palvelun toimintaan tai muihin K�ytt�jiin; </li>
                    <li> tiedostoja, linkkej� tai muuta materiaalia, joilla pyrit��n varastamaan toisten k�ytt�jien tileilt�;
                    </li>
                    <li> mit��n ohjelmistoja, joilla ker�t��n tietoja muilta k�ytt�jilt�; </li>
                    <li> mit��n muuta mik� kiert�� tai ei noudata voimassaolevia paikallisia lakeja.</li>
                </ul>
                <p class="tool_desc">Joskus sinun on rekister�idytt�v� Verkkosivulle ja/tai muihin WeLiven kautta kehitettyihin ja yll�pidettyihin
                    sovelluksiin, jotta voit k�ytt�� niit�. K�ytt�j� sitoutuu antamaan oikeat tiedot rekister�intiprosessin
                    aikana ja Palvelun niit� pyyt�ess�. Sinun on p�ivitett�v� tiedot aina kun niihin tulee muutoksia. Mik�li
                    ep�ilemme tietojesi todenmukaisuutta, meill� on oikeus lakkauttaa tilisi. Jos k�ytt�j� lakkauttaa WeLive-tilins�,
                    kaikki h�nen tarjoamansa artefaktit anonymisoidaan eli linkit tilins� lakkauttaneeseen k�ytt�j��n poistetaan
                    Viitekehyksest�. </p>
                <p class="tool_desc">WeLive-viitekehys tarjoaa julkisen ohjelmointirajapinnan ("Rajapinta") menetelmin, jotka mahdollistavat k�ytt�jien
                    statuksen, ideoitten ja artefaktien vaihtamisen alustalla. Vain k�ytt�j�t, jotka ovat rekister�ityneet
                    Viitekehykseen ja joiden profiili on tallennettuna Citizen Data Vault ("CDV") -komponentissa, voivat
                    k�ytt�� n�it� toimintoja. </p>
                <p class="tool_desc">Ehtojen rikkominen johtaa k�ytt�j�tilin perumiseen ja uuden tilin luomisen est�miseen ja voimme my�s k�ytt��
                    joitakin menetelmi� est��ksemme kielletty� k�ytt�j�� k�ytt�m�st� Sovellusta jatkossa. </p>
                <p class="tool_desc">Voimme muuttaa Palvelua koska tahansa lis��m�ll�, muokkaamalla tai poistamalla jotakin. Pyrimme kertomaan
                    muutoksista kohtuullisella tavalla. Jos jatkat Palvelun k�ytt��, katsomme sinun hyv�ksyv�n muutokset
                    ja Ehdot. </p>

                <p class="tool_tit">3. PALVELUSTA MAKSAMINEN</p>
                <p class="tool_desc">Verkkosivu, Viitekehys, Sovellus, Rajapinta, julkiset palvelut ja mik� tahansa muu WeLive-konsortion tarjoama
                    artefakti on ilmainen kaksivaiheisen pilotin aikana. Kolmansien osapuolten tarjoamien julkisten palvelujen,
                    sovellusten, rakennuspalikoiden tai muiden artefaktien k�ytt��n voi liitty� maksu. Mahdolliset maksut
                    n�ist� kolmannen osapuolen palveluista veloitetaan suoraan palveluntarjoajan toimesta WeLive-viitekehyksen
                    ulkopuolella eik� WeLive vastaa n�ist� maksuista mill��n tavalla. </p>
                <p class="tool_desc">Hankkeen loppupuolella m��ritell��n artefaktien maksuperiaate. Jos p��t�mme muuttaa Palvelun tai osan siit�
                    maksulliseksi hankkeen p��tytty�, kerromme siit� ja hinnoista kohtuullisella tavalla ja kohtuullisessa
                    ajassa, jotta k�ytt�j�t voivat lakata k�ytt�m�st� Palvelua, mik�li eiv�t halua maksaa siit�. </p>
                <p class="tool_desc">Sovellus voi kaivata yhteytt� tietoliikenneverkkoon toimiakseen oikein. Verkkoyhteys voi tuoda lis�kuluja
                    dataverkon k�ytt�misest�. N�m� kulut eiv�t ole kytk�ksiss� WeLiveen ja K�ytt�j� huolehtii niist� itse.
                </p>

                <p class="tool_tit">4. TIETOSUOJA</p>

                <p class="tool_desc">Ennen Palvelun k�ytt�mist� k�ytt�jien on hyv�ksytt�v� Suostumuslomake, joka tarjotaan heille Sovellusta tai
                    Verkkosivua k�ytett�ess� ensimm�isen kerran. Kun t�yt�t rekister�intilomakkeen Verkkosivulla tai jossakin
                    WeLive-sovelluksessa, annat henkil�tietoja (ei arkaluonteisia) ja odotat meid�n k�sittelev�n niit� huolellisesti.
                    WeLive Palveluun liittyv�t sovellukset tai Verkkosivu eiv�t koskaan pyyd� sinua antamaan arkaluonteisia
                    tietoja (lis�tietoja arkaluonteisiksi luettavista tiedoista l�yd�t directiivin 95/46/EY artiklasta 8
                    - https://secure.edps.europa.eu/EDPSWEB/webdav/site/mySite/shared/Documents/EDPS/DataProt/Legislation/Dir_1995_46_FI.pdf).
                    Konsortio (TECNALIA Research and Innovation ja muut partnerit) k�sittelee niit� sopivalla tavalla ja
                    aina noudattaen kansallisia s��d�ksi� tai soveltuvin osin tulevaa EU:n tietosuojalakia, joka tulee voimaan
                    vuonna 2018. </p>
                <p class="tool_desc">Lis�ksi K�ytt�j�n on suojeltava tili��n k�ytt�m�ll� siihen tarkoitettuja suojauskeinoja, joihin lukeutuvat:
                    Vahvan salasanan luominen; Salasanan vaihtaminen usein; Se, ettei jaa kirjautumistietoja; jne. </p>
                <p class="tool_desc">Konsortio k�ytt�� K�ytt�jien tarjoamia tietoja Verkkosivun, Sovelluksen, Viitekehyksen ja WeLive-artefaktien
                    hallintaan. Emme myy k�ytt�jien tietoja.</p>
                <p class="tool_desc">Konsortio ker�� k�ytt�jilt� henkil�tietoja, aina k�ytt�j�n luvalla, palvelun k�ytt�misen aikana. N�it� tietoja
                    s�ilytet��n CDV-komponentissa, joka varmistaa, ett� ainoastaan hallittu ja aiemmin auktorisoitu p��sy
                    sallitaan. Ker�tyt yksityiset tiedot anonymisoidaan ja kootaan yhteen, jotta Palvelun k�yt�st� saadaan
                    ker�tty� tilastotietoa. Ainoastaan WeLive-viitekehyksen sis�iset komponentit pystyv�t lukemaan ker�ttyj�
                    lokitietoja, joissa k�ytt�j�t tunnistetaan pelk�st��n heille luodun yksil�iv�n ID-tunnisteen perusteella,
                    mink� avulla eri vaiheissa ker�ttyj� tietoja pystyt��n yhdistelem��n. WeLiven tarjoamien ty�kalujen ja
                    artefaktien k�yt�st� ker�ttyjen tietojen avulla voidaan luoda mittareita ja visualisointeja viitekehyksen
                    ja sen palveluiden hyv�ksynn�n ja k�yt�n mittaamiseksi. Mittareissa tai visualisoinneissa ei n�ytet�
                    k�ytt�jien tunnistetietoja. Ker�ttyj� tietoja k�ytet��n Viitekehyksen ja Palveluiden parantamiseen tai
                    eri ty�kalujen ja komponenttien k�yt�n arviointiin. Tietoa voidaan my�s hy�dynt�� suositus- ja analysointik�ytt��n
                    WeLivess�. Lis�ksi henkil�tietoja (s�hk�postiosoitteita) k�ytet��n k�ytt�jien kanssa kommunikointiin
                    pilottiarvioinnin aikana. Henkil�tietoja ei koskaan luovuteta kolmansille osapuolille ellei k�ytt�j�
                    selv�sti anna siihen lupaansa. Arkaluonteisten henkil�tietojen tallentaminen WeLive-komponentteihin on
                    erityisesti kielletty�. WeLive-viitekehykseen perustuvat sovellukset ja palvelut eiv�t saa k�ytt�� WeLive-komponentteja
                    arkaluonteisten henkil�tietojen tallennukseen. Arkaluonteisten henkil�tietojen m��ritelm�n� sovellamme
                    EU:n antamia suosituksia aiheesta1. WeLive-yhteensopivien sovellusten kehitt�jien ja k�ytt�jien tulee
                    sitoutua noudattamaan n�iss� Ehdoissa annettuja rajoitteita arkaluonteisten henkil�tietojen tallentamisessa
                    WeLive-komponentteihin. K�ytt�j�t voivat kustomoida Sovelluksen k�ytt�� ja hakua erilaisia suodattimia
                    k�ytt�m�ll� (jotka voivat vaatia henkil�tietoja). N�it� ei ker�t� mist��n syyst�. </p>

                <p class="tool_tit">5. DATAN OMISTUSOIKEUS</p>
                <p class="tool_desc">Palvelussa tarjotut digitaaliset tietoaineistot ("Data"), jotka ovat k�ytett�viss� Verkkosivun, Sovelluksen
                    tai mink� tahansa muun WeLive-palveluymp�rist�n osan kautta, tulevat julkisesti k�ytett�viss� olevista
                    l�hteist� tai niiden omistaja on sallinut niiden yleisen k�yt�n. Datan alkuper�iset omistajat s�ilytt�v�t
                    datan omistusoikeuden ja hallinnan yhdess� Viitekehyksen yll�pit�jien kanssa. WeLive-j�rjestelm�ss� on
                    my�s mahdollista s�ilytt�� yksityist� dataa, johon p��sy on sallittu vain k�ytt�j�n sallimille tahoille.
                </p>
                <p class="tool_desc">K�ytt�j�t saavat ilmaisen p��syn dataan. Kaikkien t�t� dataa k�ytt�vien johdannaisteosten tulisi sallia ilmainen
                    p��sy dataan. </p>
                <p class="tool_desc">K�ytt�j�t voivat luoda uusia tietoaineistoja joko Viitekehyksen tai Sovelluksen kautta ja heill� on n�ihin
                    aineistoihin t�ysi k�ytt�oikeus. Kun k�ytt�j� luo aineiston, h�n m��rittelee onko aineisto julkisessa
                    vai yksityisess� k�yt�ss�. K�ytt�j�n toimittama data, lukuunottamatta henkil�kohtaista tai yksityist�
                    tietoa, kuuluu sek� k�ytt�j�lle ett� Konsortiolle. K�ytt�j�t s�ilytt�v�t hallinnan luomiinsa aineistoihin
                    ja voivat halutessaan poistaa ne. Aikaisempiin julkisiin tietoaineistoihin pohjautuvat k�ytt�j�n lis��m�t
                    tietoaineistot jaetaan Viitekehyksess�, eik� k�ytt�j�ll� ole mahdollisuutta poistaa niit�, elleiv�t ne
                    sis�ll� henkil�kohtaista tai yksityist� tietoa. Mik�li k�ytt�j�n tili poistetaan Palvelusta, kaikki viittaukset
                    kyseiseen k�ytt�j��n poistetaan my�s h�nen toimittamiensa tietoaineistojen l�hdetiedoista. K�ytt�j�ll�
                    on koska tahansa mahdollisuus saada kaikki henkil�kohtainen tietonsa Viitekehyksen Citizen Data Vault
                    (CDV) -komponentin kautta. K�ytt�j�ll� on my�s p��sy kaikkiin omistamiinsa aineistoihin Open Data Stack
                    (ODS) -komponentin kautta. </p>
                <p class="tool_desc">Jos aiot julkaista Viitekehyksess� muualta saatavissa olevia tietoaineistoja, tarkista ensin niiden alkuper�isist�
                    k�ytt�ehdoista, ett� sinulla on oikeus tehd� niin. Jos aiot julkaista Viitekehyksest� l�ytyvi� tietoaineistoja
                    muilla tavoilla, kuten esimerkiksi yhteis�palveluissa tai kolmansien tahojen verkkosivuilla, perehdy
                    ensin n�iden palveluiden k�ytt�ehtoihin varmistaaksesi, ettet loukkaa WeLive-k�ytt�ehtoja tai kyseisten
                    tietoaineistojen lisenssej�. Muista my�s s�ilytt�� viite alkuper�iseen tietoaineistoon uudelleenjulkaisun
                    yhteydess�. </p>
                <p class="tool_desc">Konsortio ei maksa palkkioita k�ytt�jien julkaisemasta datasta. Konsortio voi tarkistaa k�ytt�jien julkaiseman
                    datan ja poistaa sen, mik�li se on virheellist� tai ei vastaa palvelun tarkoitusta. Viitekehyksen ODS-komponentti
                    sis�lt�� mekanismeja, joilla voidaan havaita aineistojen v��rink�ytt��. </p>
                <p class="tool_desc">My�hemm�ss� vaiheessa WeLive-hanketta, tietoaineistojen toimittajilla voi olla mahdollisuus muuttaa julkaisemiensa
                    tietoaineistojen k�ytt� maksulliseksi. </p>

                <p class="tool_tit">6. DATAN TARKKUUS</p>
                <p class="tool_desc">Data toimitetaan tietosis�ll�lt��n sellaisena, kuin se on alkuper�isiss� tietol�hteiss�, mutta aineiston
                    esitysmuoto voi poiketa alkuper�isest� k�yt�n helpottamiseksi. TECNALIA ja sen WeLive-konsortion partnerit
                    eiv�t ole vastuussa datan tarkkuudesta tai saatavuudesta, eiv�tk� tiedon k�yt�st� johtuvista suorista
                    tai ep�suorista kustannuksista. Viitekehys tarjoaa mahdollisuuksia automaattiseen datan laadun ja oikeellisuuden
                    arviointiin. </p>

                <p class="tool_tit">7. KOLMANNET OSAPUOLET</p>
                <p class="tool_desc">K�ytt�j� saattaa toisinaan ohjautua Verkkosivun tai joidenkin Sovellusten kautta Konsortion ulkopuolisten
                    kolmansien osapuolien hallitsemiin palveluihin, esimerkiksi yhteis�palveluihin. TECNALIA ja sen WeLive-konsortion
                    partnerit eiv�t ole vastuussa n�iden palveluiden yksityisyyden suojasta tai mahdollisista k�ytt�kustannuksista.
                </p>
                <p class="tool_desc">Mik�li k�yt�t yhteis�palvelun k�ytt�j�tili� tunnistautuaksesi Verkkosivulle tai Sovellukseen, TECNALIA ja
                    sen WeLive-konsortion partnerit eiv�t ker�� muita tietoja kuin ne, joihin K�ytt�j� on nimenomaisesti
                    antanut luvan ja p��syn. </p>
                <p class="tool_desc">Mik�li julkaiset tietoa yhteis�palveluissa Sovelluksen tai Verkkosivun kautta, huomioi ett� t�m� tiedon julkaisu
                    tapahtuu kyseisen yhteis�palvelun yksityisyydensuojasopimuksen ja k�ytt�ehtojen alaisesti. </p>

                <p class="tool_tit">8. SOVELLUKSEN OMISTUSOIKEUS</p>
                <p class="tool_desc">Verkkosivu, Sovellus ja Viitekehys ovat Konsortion omaisuutta. Mik� tahansa muu WeLive -artefakti on sen
                    tekij�iden ja Viitekehyksen hallinnoijan eli WeLive-ratkaisun palveluntarjoajan omaisuutta. Kolmannen
                    osapuolen artefaktien tuotto, mik�li hinnoittelumalli on kyseiselle artefaktille m��ritelty, jaetaan
                    sen tekij�iden ja Viitekehyksen palveluntarjoajan kesken. </p>
                <p class="tool_desc">Tekij�t sallivat ei-yksinomaisen ja peruutettavissa olevan lisenssin kopioida, asentaa ja k�ytt�� Sovellusta
                    k�ytt�jien laitteissa ja k�ytt�� Palvelua ilman kustannuksia. T�m� lisenssi ei anna k�ytt�j�lle Sovelluksen
                    omistusoikeutta, vaan se s�ilyy tekij�ill�. </p>

                <p class="tool_tit">9. SAATAVUUS</p>
                <p class="tool_desc">TECNALIA ja sen WeLive-konsortion partrerit eiv�t sitoudu huolehtimaan Verkkosivun, Sovellusten tai tiedon
                    saatavuudesta. Edell� mainitut tahot pyrkiv�t v�ltt�m��n ja minimoimaan Palvelun katkot ja maksimoimaan
                    sen saatavuuden. Palvelun tarjoajat tiedottavat suunnitelluista palvelun katkoksista sopivaksi katsomallaan
                    tavalla ja pyrkiv�t minimoimaan katkosten keston. </p>
                <p class="tool_desc">TECNALIA ja sen WeLive-konsortion partnerit eiv�t ole vastuussa Palvelun k�ytt�katkoksista aiheutuvista vahingoista
                    eik� suorista tai ep�suorista kustannuksista. </p>

                <p class="tool_tit">10. OIKEUSTURVA</p>
                <p class="tool_desc">My�nn�mme k�ytt�j�lle ei-yksinomaisen ja peruutettavissa olevan lisenssin asentaa, kopioida ja k�ytt�� Sovellusta
                    laitteisaan ja k�ytt�� Palvelua ilman kustannuksia. </p>
                <p class="tool_desc">K�ytt�j� hyv�ksyy, ettei k�yt� Palvelua mihink��n seuraavista tarkoituksista: </p>
                <ul>
                    <li> Palvelun h�irint��n tai v��rink�ytt��n; </li>
                    <li> Datan muokkaamiseen pahansuovasti; </li>
                    <li> Tietojen ker��miseksi palvelun k�ytt�jist�; </li>
                    <li> Muiden k�ytt�jien uhkaamiseen, h�irint��n tai kirist�miseen; </li>
                    <li> Kolmansien osapuolien palveluiden h�irint��n; </li>
                    <li> Kolmansien osapuolien tekij�noikeuksien loukkaamiseen.</li>
                </ul>
                <p class="tool_desc">Sovellus tarjotaan k�ytt��n sellaisenaan ja sen k�ytt� tapahtuu omalla vastuulla. Konsortio ei ole vastuussa
                    Sovelluksen k�yt�st� aiheutuneista virheist�, vikatilanteista p��telaitteessa, muiden sovellusten k�yt�n
                    h�iri�ist�, eik� datan tai sen k�ytt�mahdollisuuksien menetyksest�.</p>

                <p class="tool_tit">11. KIELIVERSIOT</p>
                <p class="tool_desc">N�m� WeLive - k�ytt�ehdot on alunperin kirjoitettu englanniksi ja sen j�lkeen k��nnetty eri kielille, jotta
                    k�ytt�jien olisi helpompi tutustua niihin. Voit tutustua my�s Ehtojen muihin kieliversioihin vaihtamalla
                    Sovelluksen kieliasetuksia. Mik�li k��nnetyt (ei enlanninkieliset) k�ytt�ehdot ovat mill��n tavalla ristiriitaisia
                    vastaavan englanninkielisen version kanssa, englanninkielisess� versiossa m��riteltyj� ehtoja tulee soveltaa
                    ensisijaisesti. </p>

		</div>
	</div>
</div>	