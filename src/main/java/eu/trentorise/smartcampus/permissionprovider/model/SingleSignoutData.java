/**
 * Copyright 2016 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.permissionprovider.model;

import java.io.Serializable;

public class SingleSignoutData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** protocol type. **/
	private String authProtocolType;
	/** session identified (ticket | token). **/
	private String sessionIdentifier;
	/** client redirect. **/
	private String redirectUrl;

	public SingleSignoutData(String protocolType, String sessionIdentifier, String redirectUrl) {
		super();
		this.authProtocolType = protocolType;
		this.sessionIdentifier = sessionIdentifier;
		this.redirectUrl = redirectUrl;
	}

	public String getProtocolType() {
		return authProtocolType;
	}

	public void setProtocolType(String protocolType) {
		this.authProtocolType = protocolType;
	}

	public String getSessionIdentifier() {
		return sessionIdentifier;
	}

	public void setSessionIdentifier(String sessionIdentifier) {
		this.sessionIdentifier = sessionIdentifier;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	@Override
	public String toString() {
		return "SingleSignoutData [protocolType=" + authProtocolType + ", sessionIdentifier=" + sessionIdentifier
				+ ", redirectUrl=" + redirectUrl + "]";
	}

}
