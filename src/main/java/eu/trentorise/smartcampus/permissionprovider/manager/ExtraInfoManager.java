/**
 * Copyright 2016 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.permissionprovider.manager;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import eu.trentorise.smartcampus.network.RemoteException;
import eu.trentorise.smartcampus.permissionprovider.beans.ExtraInfoBean;
import eu.trentorise.smartcampus.permissionprovider.controller.AuthController;
import eu.trentorise.smartcampus.permissionprovider.model.ExtraInfo;
import eu.trentorise.smartcampus.permissionprovider.model.User;
import eu.trentorise.smartcampus.permissionprovider.repository.ExtraInfoRepository;
import eu.trentorise.smartcampus.permissionprovider.repository.UserRepository;

@Component
@Transactional(rollbackFor = Exception.class)
public class ExtraInfoManager {

	private static Log normalLogger = LogFactory.getLog(ExtraInfoManager.class);
	
	private static final String DEFAULT_ROLE = "Citizen";
	
	@Autowired
	private WeLiveLogger logger;
	
	@Autowired
	private AttributesAdapter attrAdapter;
	
	@Autowired
	private ExtraInfoRepository infoRepo;

	@Autowired
	private UserRepository userRepo;

	@Value("${api.token}")
	private String token;

	@Value("${welive.lum}")
	private String lumEndpoint;

	public boolean infoAlreadyCollected(Long userId) {
		User load = userRepo.findOne(userId);
		return load != null && infoAlreadyCollected(load);
	}

	public boolean infoAlreadyCollected(User user) {
		return infoRepo.findByUser(user) != null;
	}

	public User collectInfoForUser(ExtraInfoBean info, User newUser) throws SecurityException, RemoteException {
//		User load = userRepo.findOne(userId);
		if (newUser != null) {
			if (info != null) {
				/** save user. issue#344 **/
				newUser = userRepo.save(newUser);
				normalLogger.info("user created: " + newUser);
				if (!StringUtils.hasText(newUser.email())) {
					addEmail(newUser, info.getEmail());
				}
				
				sendAddUser(info, newUser.getId());
				
				ExtraInfo entity = createEntity(info);
				entity.setUser(newUser);
				
				infoRepo.save(entity);
				
				if(info.getPilot() != null) {
					Map<String,Object> logMap = new HashMap<String, Object>();
					logMap.put("userid", ""+newUser.getId());
					logMap.put("pilot", info.getPilot());
					logger.log(WeLiveLogger.USER_EXTRA_INFO, logMap);
				}
			}
		}
		return newUser;
	}

	/**
	 * @param user
	 * @param email
	 */
	private void addEmail(User user, String email) {
		user.updateEmail(email);
		userRepo.save(user);
	}

	/**
	 * @param info
	 * @return
	 */
	private ExtraInfo createEntity(ExtraInfoBean info) {
		ExtraInfo entity = info == null ? new ExtraInfo(): new ExtraInfo(info);
		if (!StringUtils.hasText(entity.getRole())) {
			entity.setRole(DEFAULT_ROLE);
		}
		
		return entity;
	}

	/**
	 * @param info
	 * @throws RemoteException 
	 * @throws SecurityException 
	 */
	private void sendAddUser(ExtraInfoBean info, Long userId) throws SecurityException, RemoteException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ccUserId", userId);
		map.put("pilot", info.getPilot());
		map.put("firstName", info.getName());
		map.put("surname", info.getSurname());
		map.put("email", info.getEmail());

		// default initialization -> issue#286.
//		map.put("isMale", false);
		if (info.getGender() != null && !info.getGender().isEmpty()) {
			map.put("isMale", !"F".equals(info.getGender()));
		}

		if (info.getBirthdate() != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(info.getBirthdate());
			map.put("birthdayDay", c.get(Calendar.DAY_OF_MONTH));
			map.put("birthdayMonth", c.get(Calendar.MONTH) + 1);
			map.put("birthdayYear", c.get(Calendar.YEAR));
		}

		map.put("developer", info.isDeveloper());
		map.put("address", info.getAddress());
		map.put("zipCode", info.getZip());
		map.put("city", info.getCity());
		map.put("country", info.getCountry());
		if (info.getLanguage() != null)
			map.put("languages", info.getLanguage());
		map.put("role", StringUtils.hasText(info.getRole()) ? info.getRole() : "Citizen");
		map.put("employement", info.getStatus());
		if (info.getKeywords() != null) {
			map.put("tags", StringUtils.commaDelimitedListToStringArray(info.getKeywords()));
		}
		
//		map.put("isAdult", info.isAdult());
		
//		map.put("cmd", "{\"/Challenge62-portlet.clsidea/add-new-user\":{}}");
//		String postJSON = call("https://dev.welive.eu/api/jsonws/invoke", map, Collections.<String,String>singletonMap("Authorization", "Basic " + token));
		String postJSON = call(lumEndpoint, map, Collections.<String,String>singletonMap("Authorization", "Basic " + token));
		System.err.print(postJSON);
		
	}
	
	private static HttpClient getDefaultHttpClient(HttpParams inParams) {
		if (inParams != null) {
			return new DefaultHttpClient(inParams);
		} else {
			return new DefaultHttpClient();
		}
	}
	private static String call(String url, Map<String,Object> body, Map<String,String> headers) throws RemoteException {
		final HttpResponse resp;
		final HttpPost post = new HttpPost(url);

		post.setHeader("Accept", "application/json");
		post.setHeader("Content-Type", "application/json");
		if (headers != null) {
			for (String key : headers.keySet()) {
				post.setHeader(key, headers.get(key));
			}
		}		

		try {
			String bodyStr = new ObjectMapper().writeValueAsString(body);
			StringEntity input = new StringEntity(bodyStr, "UTF-8");
			normalLogger.info(url);
			normalLogger.info(bodyStr);
//			input.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
			input.setContentType("application/json; charset=UTF-8");
			post.setEntity(input);

			resp = getDefaultHttpClient(null).execute(post);
			String response = EntityUtils.toString(resp.getEntity(),"UTF-8");
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				Map<String,Object> respMap = new ObjectMapper().readValue(response, HashMap.class);
				if (respMap != null
						&& ( StringUtils.hasText((String)respMap.get("exception")) 
								|| Boolean.TRUE.equals(respMap.get("error")))) 
				{
					throw new RemoteException((String)respMap.get("exception"));
				}
				return response;
			}
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN
					|| resp.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
				throw new SecurityException();
			}

			String msg = "";
			try {
				msg = response.substring(response.indexOf("<h1>") + 4,
						response.indexOf("</h1>", response.indexOf("<h1>")));
			} catch (Exception e) {
				msg = resp.getStatusLine().toString();
			}
			throw new RemoteException(msg);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}
}
